import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


import { UserInfos } from './../../interfaces/user-infos';
import { NgForm } from '@angular/forms';
import { UserData } from 'src/app/providers/user-data.service';

@Component({
  selector: 'app-username',
  templateUrl: './username.page.html',
  styleUrls: ['./username.page.scss'],
})
export class UsernamePage implements OnInit {
  public userInfos: UserInfos = { username: '' };

  constructor(
    public user: UserData,
    public router: Router,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  onSaveUserName(form: NgForm) {
    console.warn('form', form);
    if (form.valid) {
      this.storage.set('pseudo', this.user.login(this.userInfos.username));
      this.router.navigateByUrl('charleston/Charleston', { replaceUrl: true });
    }
  }

}
