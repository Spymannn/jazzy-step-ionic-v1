import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CharlestonPageRoutingModule } from './charleston-routing.module';

import { CharlestonPage } from './charleston.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CharlestonPageRoutingModule
  ],
  declarations: [CharlestonPage]
})
export class CharlestonPageModule {}
