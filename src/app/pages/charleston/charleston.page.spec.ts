import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CharlestonPage } from './charleston.page';

describe('CharlestonPage', () => {
  let component: CharlestonPage;
  let fixture: ComponentFixture<CharlestonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharlestonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CharlestonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
