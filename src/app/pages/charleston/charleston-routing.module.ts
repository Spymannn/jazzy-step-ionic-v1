import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CharlestonPage } from './charleston.page';

const routes: Routes = [
  {
    path: '',
    component: CharlestonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CharlestonPageRoutingModule {}
