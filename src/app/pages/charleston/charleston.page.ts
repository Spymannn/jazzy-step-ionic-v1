import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as charlestonStep from './charlestonStep.json';


@Component({
  selector: 'app-charleston',
  templateUrl: './charleston.page.html',
  styleUrls: ['./charleston.page.scss'],
})
export class CharlestonPage implements OnInit {
  public title: string;
  public charlestonSteps: any[];

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.title = this.activatedRoute.snapshot.paramMap.get('name');
    this.charlestonSteps = charlestonStep.steps;
    console.warn('charlestonStep', charlestonStep);
  }

}
