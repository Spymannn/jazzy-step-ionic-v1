import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UserData } from './providers/user-data.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'charleston/:name',
    loadChildren: () => import('./pages/charleston/charleston.module').then( m => m.CharlestonPageModule)
  },
  {
    path: 'username',
    loadChildren: () => import('./pages/username/username.module').then(m => m.UsernamePageModule),
    canLoad: [UserData]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
