import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class UserData implements CanLoad {
    IS_USER_LOGGED = 'isUserLogged';
  constructor(private storage: Storage, private router: Router) {}

  canLoad() {
    return this.storage.get('username').then(res => {
      if (res) {
        this.router.navigateByUrl('', { replaceUrl: true });
        return false;
      } else {
        return true;
      }
    });
  }

  login(username: string): Promise<any> {
    return this.storage.set(this.IS_USER_LOGGED, true).then(() => {
      this.setUsername(username);
      return window.dispatchEvent(new CustomEvent('user:login'));
    });
  }

  setUsername(username: string): Promise<any> {
    return this.storage.set('username', username);
  }

  getUsername(): Promise<string> {
    return this.storage.get('username').then((value) => {
      return value;
    });
  }

  isLoggedIn(): Promise<boolean> {
    return this.storage.get(this.IS_USER_LOGGED).then((value) => {
      return value === true;
    });
  }
}
