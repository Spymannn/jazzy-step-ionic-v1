import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserData } from './providers/user-data.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Charleston',
      url: '/charleston/Charleston',
      icon: 'bug'
    },
    {
      title: 'Solo Jazz',
      url: '/folder/SoloJazz',
      icon: 'bonfire'
    },
    {
      title: 'Routines',
      url: '/folder/Routines',
      icon: 'heart'
    },
    {
      title: 'Contact',
      url: '/folder/Contact',
      icon: 'information'
    }
  ];
  public labels = ['Charleston', 'Solo Jazz', 'Routines', 'Contact'];
  public userName = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private user: UserData
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = 'charlestonCharleston';
    this.user.getUsername().then(name => {
      console.warn('username', name);
      this.userName = name;
    });

    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
